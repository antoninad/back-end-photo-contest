/* eslint-disable max-len */
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import passport from 'passport';
import { PORT } from './config.js';
import contestsController from './controllers/contests-controller.js';
import authController from './controllers/auth-controller.js';
import jwtStrategy from './auth/strategy.js';
import usersController from './controllers/users-controller.js';
import photosController from './controllers/photos-controller.js';

const app = express();
passport.use(jwtStrategy);

app.use(bodyParser.json())
app.use(cors(), helmet());
app.use(passport.initialize());

app.use('/auth', authController);
app.use('/contests', contestsController);
app.use('/users', usersController);
app.use('/photos', photosController);
app.use('/public', express.static('images'));

app.use((err, req, res, next) => {
  // logger.log(err)

  res.status(500).send({
    message: 'Server error occurred. Our developers are working hard to resolve it.'
  });
})

app.all('*', (req, res) => {
  res.status(404).send({ message: 'Resource not found!' });
});

app.listen(PORT, () => console.log(`App is listening on port: ${PORT}`));
