/* eslint-disable max-len */
import passport from 'passport';
import { JWT_STRATEGY_NAME } from '../config.js';
import userData from '../data/users-data.js';

// Authentication middleware
const authMiddleware = passport.authenticate(
    JWT_STRATEGY_NAME,
    { session: false }
);

// Authorization middleware
const roleMiddleware = (roleName) => {
  return (req, res, next) => {
    if (req.user && (req.user.role === roleName)) {
      next()
    } else {
      res.status(403).send({
        message: 'This resource is forbidden.'
      })
    }
  };
};

// Black listed token middleware
const tokenMiddleware = () => {
  return async (req, res, next) => {
    const isTokenBlacklisted = await userData.getToken(req.headers.authorization);
    if (isTokenBlacklisted[0]) {
      res.status(400).send({ error: 'Bad Request/Invalid Session' })
    } else {
      next()
    }
  }
}

export {
  authMiddleware,
  roleMiddleware,
  tokenMiddleware
};
