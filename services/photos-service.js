/* eslint-disable max-len */
import serviceErrors from './error-service.js';

const getAllPhotos = (photoData) => {
  return async (filter) => {
    return filter ?
        await photoData.searchBy('title', filter) :
        await photoData.getAll();
  }
};

const getAllUsersPhotos = (photoData) => {
  return async (userId) => {
    const photos = await photoData.getUsersAll(userId);
    return photos;
  }
};

const createReview = (photoData) => {
  return async (id, data, userId) => {
    const { score, comment, checkbox } = data;
    const checkIfUserHasReviewd = await photoData.checkIfReviewed(id, userId);

    if (checkIfUserHasReviewd) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        review: null
      }
    }

    if (checkbox === 1) {
      //
    }

    const postReview =
    await photoData.review(score, comment, checkbox, userId, id);

    if (!postReview) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        review: null,
      }
    }
    return {
      error: null,
      review: postReview
    }
  }
}

const checkIfHasWroteReview = (photoData) => {
  return async (id, userId) => {
    const checkIfReviewed = await photoData.reviewFromUser(id, userId);

    if (!checkIfReviewed) {
      return {
        error: null,
        reviews: 'not reviewd',
        photoId: id
      }
    }
    return {
      error: null,
      reviews: 'reviewd',
      photoId: id
    }
  }
}

const getAllPhotoReviews = (photoData) => {
  return async (photoId) => {
    const reviews = await photoData.getReviews(+photoId);
    return reviews;
  }
};

export default {
  getAllPhotos,
  createReview,
  checkIfHasWroteReview,
  getAllPhotoReviews,
  getAllUsersPhotos
}
