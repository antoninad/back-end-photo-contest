/* eslint-disable max-len */
import serviceErrors from './error-service.js';
import bcrypt from 'bcrypt';
import rankingPoints from '../constants/rankingPoints.js';

const signIn = (userData) => {
  return async (username, password) => {
    const user = await userData.getSignInInfo(username);

    if (!user ||
      !(await bcrypt.compare(password, user.password)) ||
      user.is_deleted === 1) {
      return {
        error: serviceErrors.INVALID_SIGNIN,
        user: null
      }
    }
    return {
      error: null,
      user: user
    }
  }
}

const signUp = (userData) => {
  return async (userCreate) => {
    const { username, password, firstName, lastName } = userCreate;

    const existingUser = await userData.getSignInInfo(username);

    if (existingUser) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null
      }
    }

    const passwordHash = await bcrypt.hash(password, 10);
    const user = await userData.createUser(
        username,
        passwordHash,
        firstName,
        lastName
    );

    return { error: null, user: user };
  }
};

const addTokenToBlackList = (usersData) => {
  return async (token) => {
    const blackListedToken = await usersData.getToken(token);

    if (blackListedToken[0]) {
      return blackListedToken[0];
    }
    const newToken = await usersData.addToken(token)
    return newToken[0];
  };
}

const getByRank = (userData) => {
  return async () => {
    const users = await userData.getOrderedByRank();

    if (!users) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        users: null
      }
    }
    return {
      error: null,
      users: users
    }
  }
}

const getScoringInfo = (userData) => {
  return async (userId) => {
    const info = await userData.getPointsAndRanking(userId, 'NaN');
    const left = rankingPoints.map((el) => {
      if (el.type === info.type) {
        const points = el.maxPoints - info.currPoints + 1;
        info.leftPoints = points;
      }
      return el.type;
    });
    if (!info) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        info: null
      }
    }
    return {
      error: null,
      info: info
    }
  }
}

const updateAvatar = (userData) => {
  return async (id, path) => {
    const result = await userData.updateAvatar(id, path);
    return { error: result.affectedRows > 0 ? null : serviceErrors.UNSPECIFIED_ERROR };
  }
};

const getAllUsers = (userData) => {
  return async () => {
    const result = await userData.getAll();
    if (!result) {
      return { error: serviceErrors.RECORD_NOT_FOUND, users: null }
    }
    return { error: null, users: result }
  }
}

const getWithMasterRank = (userData) => {
  return async () => {
    const result = await userData.getMasterRanked();
    if (!result) {
      return { error: serviceErrors.RECORD_NOT_FOUND, users: null }
    }
    return { error: null, users: result }
  }
}

const getUserByRank = (usersData) => {
  return async (rankId) => {
    const user = await usersData.getByRank(rankId);

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null
      };
    }

    return { error: null, user: user };
  }
};

const getInvitedAsJuryContests = (contestData) => {
  return async (userId) => {
    const result = await contestData.getInvitedAsJury(userId);

    return { error: null, contests: result }
  }
}

const checkIfJury = (userData) => {
  return async (contestId, userId) => {
    const result = await userData.isJury(contestId, userId);

    return { error: null, contests: result }
  }
}

const getUntilNextRank = (rankingPoints, userData) => {
  return async (userId) => {
    const currPoints = await userData.getPointsAndRanking(userId, userId);
    let pointsLeft = 0;
    let nextRank = '';

    if (currPoints.currPoints > rankingPoints[3].minPoints) {
      nextRank = rankingPoints[3].type;
    }

    if (currPoints.currPoints >= rankingPoints[2].minPoints && currPoints.currPoints <= rankingPoints[2].maxPoints) {
      nextRank = rankingPoints[3].type;
      pointsLeft = rankingPoints[2].maxPoints + 1 - currPoints.currPoints;
    }

    if (currPoints.currPoints >= rankingPoints[1].minPoints && currPoints.currPoints <= rankingPoints[1].maxPoints) {
      nextRank = rankingPoints[2].type;
      pointsLeft = rankingPoints[1].maxPoints + 1 - currPoints.currPoints;
    }

    if (currPoints.currPoints >= rankingPoints[0].minPoints && currPoints.currPoints <= rankingPoints[0].maxPoints) {
      nextRank = rankingPoints[1].type;
      pointsLeft = rankingPoints[0].maxPoints + 1 - currPoints.currPoints;
    }

    return { pointsLeft: pointsLeft, nextRank: nextRank };
  }
};

const getAvatarName = (userData) => {
  return async (userId) => {
    const currAvatar = await userData.getAvatar(userId);

    return { currAvatar: currAvatar }
  }
}

export default {
  signIn,
  signUp,
  addTokenToBlackList,
  getByRank,
  getScoringInfo,
  updateAvatar,
  getAllUsers,
  getWithMasterRank,
  getUserByRank,
  getInvitedAsJuryContests,
  checkIfJury,
  getUntilNextRank,
  getAvatarName
}
