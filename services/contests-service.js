import serviceErrors from './error-service.js';

const getPhase = (contestData) => {
  return async (id) => {
    const contests = await contestData.getPh(id);

    if (id > 3) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        contests: null
      }
    }
    return { error: null, contests: contests }
  }
}

const getPrevContests = (contestData) => {
  return async (userId) => {
    const contests = await contestData.getPrevious(userId);
    if (!contests) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contests: null
      }
    }
    return { error: null, contests: contests }
  }
}

const createContest = (contestData, userData) => {
  return async (
      title,
      category,
      typeId,
      ph1LimitDays,
      ph2LimitHours,
      selectJury,
      invitedUsers,
      userId,
      imageUrl) => {
    const existContest = await contestData.getBy('title', title);

    if (invitedUsers && +typeId === 1) {
      return {
        // eslint-disable-next-line max-len
        error: 'Users can be invited only to contests of type 2 (invitational), if your contest is of type 1 leave empty string',
        newContest: null
      }
    }

    if (invitedUsers.length === 0 && +typeId === 2) {
      return {
        error: 'For contest of type 2 you should invite participants!',
        newContest: null
      }
    }

    if (existContest) {
      return {
        error: 'Contest with this title already exists.',
        newContest: null
      }
    }

    const selectJuryUsernames = selectJury.split(',');
    const candidateJuryIds = [];
    const fututreContestId = await contestData.nextContestId();

    if (selectJury) {
      for (const username of selectJuryUsernames) {
        const checkUsersRank = await userData.getPointsAndRanking(0, username);

        if (checkUsersRank && checkUsersRank.type !== 'Master' &&
          checkUsersRank.type !== 'Wise and Benevolent Photo Dictator') {
          return {
            error: `User with username '${username}' can not be added as jury.`,
            newContest: null
          }
        }
        candidateJuryIds.push({
          username: username,
          userId: checkUsersRank.id
        });
      }
    }

    candidateJuryIds.map(async (el) => {
      // ERROR if two identical usernames are given
      const makeJury =
        await userData.addAsJury(el.userId, fututreContestId.nextId);
    })

    const newContest = await contestData.create(
        title, category, typeId, ph1LimitDays, ph2LimitHours, userId, imageUrl
    );

    const invitedUsersArr = invitedUsers.split(',');
    if (invitedUsers && +typeId === 2) {
      for (const username of invitedUsersArr) {
        const checkUser = await userData.getPointsAndRanking(0, username);
        if (!checkUser) {
          return `User '${username} does not exist.`;
        }
        const makeInvited =
          await userData.addAsInvited(checkUser.id, newContest.insertId);

        const point = await contestData.addPointsIfInvited(checkUser.id);
      }
    }

    return {
      error: null,
      newContest: newContest,
    };
  }
};

const getOpenContests = (contestData) => {
  return async (userId) => {
    const allOpen = await contestData.getOpen();
    const isJoined = await contestData.getCurr(userId);

    if (!allOpen) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        newContest: null,
      };
    }
    return allOpen;
  }
};

const getInvitaionalContests = (contestData) => {
  return async (userId) => {
    const contests = await contestData.getInvitational(userId);
    if (contests.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contests: null
      }
    }
    return {
      error: null,
      contests: contests
    }
  }
}

const getCurrContests = (contestData) => {
  return async (userId) => {
    const contests = await contestData.getCurr(userId);
    if (contests.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contests: null
      }
    }
    return { error: null, contests: contests }
  }
};

const getPhotosForContest = (contestData) => {
  return async (contestId) => {
    const photos = await contestData.getPhotos(contestId);

    /* empty array means that there are no photos in this contest,
    so it is not an error */
    return { error: null, photos: photos }
  }
};

const getReviewForPhoto = (contestData) => {
  return async (contestId, photoId) => {
    const reviews = await contestData.getReview(contestId, photoId);
    if (reviews.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        reviews: null
      }
    }
    return { error: null, reviews: reviews }
  }
};

const getIndividualContest = (contestData, userData) => {
  return async (contestId, userId) => {
    const contest = await contestData.getIndividual(contestId);
    const jury = await userData.isJury(contestId, userId);
    if (contest.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contest: null
      }
    }
    return { error: null, contest: { data: contest, jury: !!jury } }
  }
};

const getSubmittedPhotos = (contestData) => {
  return async (contestId) => {
    const photos =
      await contestData.getPhotosInContest(contestId);

    if (photos.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        photos: null
      }
    }
    return { error: null, photos: photos }
  }
};

const uploadPhoto = (contestData) => {
  return async (imageUrl, title, story, userId, contestId) => {
    const check = await contestData.checkIfUploaded(userId, contestId);
    if (check) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
      }
    }

    const contest = await contestData.getIndividual(contestId);
    if (contest.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contest: null
      }
    }
    const result =
    await contestData.upload(imageUrl, title, story, userId, contestId);

    return { error: result.affectedRows > 0 ?
      null : serviceErrors.OPERATION_NOT_PERMITTED };
  }
};

const joinToContest = (contestData, userData) => {
  return async (userId, contestId) => {
    const isInvited = await contestData.ifIsInvited(contestId, userId);
    const isJoined = await contestData.ifIsJoined(userId, contestId);

    const checkTypeAndPhase =
    await contestData.getIndividualFullInfo(contestId);

    if (checkTypeAndPhase[0]) {
      if (isInvited && checkTypeAndPhase[0].types_id === 2 &&
          !isJoined && checkTypeAndPhase[0].phase_id === 1) {
        const join = await contestData.joinContest(userId, contestId);
        const point = await contestData.addPointsWhenJoin(userId);
        return {
          error: null
        }
      }

      if (checkTypeAndPhase[0].types_id === 1 && !isJoined &&
          checkTypeAndPhase[0].phase_id === 1) {
        const join = await contestData.joinContest(userId, contestId)
        const point = await contestData.addPointsWhenJoin(userId);
        return {
          error: null
        }
      }
    }

    return {
      error: serviceErrors.RECORD_NOT_FOUND
    }
  }
};

const canUserJoinToContest = (contestData, userData) => {
  return async (userId, contestId) => {
    const isInvited = await contestData.ifIsInvited(contestId, userId);
    const isJoined = await contestData.ifIsJoined(userId, contestId);
    const checkTypeAndPhase =
    await contestData.getIndividualFullInfo(contestId);

    if (checkTypeAndPhase[0]) {
      if (isInvited && checkTypeAndPhase[0].types_id === 2 &&
          !isJoined && checkTypeAndPhase[0].phase_id === 1) {
        return {
          result: true
        }
      }

      if (checkTypeAndPhase[0].types_id === 1 && !isJoined &&
          checkTypeAndPhase[0].phase_id === 1) {
        return {
          result: true
        }
      }
    }

    return {
      result: false
    }
  }
};

const canUserUploadPhoto = (contestData) => {
  return async (userId, contestId) => {
    const isInvited = await contestData.ifIsInvited(contestId, userId);
    const isJoined = await contestData.ifIsJoined(userId, contestId);
    const checkTypeAndPhase =
    await contestData.getIndividualFullInfo(contestId);

    if (isJoined) {
      if (isJoined.has_uploaded === 0) {
        return {
          result: true
        }
      }
    }


    return {
      result: false
    }
  }
};

const winnerPhotos = (contestData) => {
  return async (contestId) => {
    const photos = await contestData.getWinnerPhotos(contestId);

    // photos.length === 0 is NOT an error
    return {
      photos: photos
    }
  }
}

export default {
  getPhase,
  getPrevContests,
  createContest,
  getOpenContests,
  getInvitaionalContests,
  getCurrContests,
  getPhotosForContest,
  getReviewForPhoto,
  getIndividualContest,
  getSubmittedPhotos,
  uploadPhoto,
  joinToContest,
  canUserJoinToContest,
  canUserUploadPhoto,
  winnerPhotos,
}
