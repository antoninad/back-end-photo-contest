/* eslint-disable max-len */
import pool from './pool.js';

const getAll = async () => {
  const sql = `
    select p.id as photo_id, p.image_url, p.title,
    c.title as contest_title, r.score, r.comment
    from reviews r
    join photos p
    on p.id = r.photo_id
    join contests c
    where c.is_active = 1; 
    `;
  const result = await pool.query(sql);
  return result;
};

const getUsersAll = async (userId) => {
  const sql = `
    select u.id as user_id, u.username, p.id as photo_id, p.image_url, p.title, p.story, c.id as contest_id
    from photos p
    join users u
    on u.id = p.users_id
    join contests c
    on c.id = p.contest_id
    where u.id = ${userId};
    `;
  const result = await pool.query(sql);
  return result;
};

const searchBy = async (column, value) => {
  const sql = `
  select p.id as photo_id,
  p.image_url, p.title, p.story, u.username, c.title as contest_title
  from photos p
  join contests c
  on c.id = p.contest_id
  join users u 
  on u.id = p.users_id
  where p.${column} like '%${value}%';
  `;
  const result = await pool.query(sql);
  // eslint-disable-next-line consistent-return
  const res = result.map((photo) => {
    if (photo.photo_id) {
      return photo;
    }
  })
  return res;
};

const checkIfReviewed = async (id, userId) => {
  const sql = `
  select *
  from reviews
  where users_id = ?
  and photo_id = ?;
  `;
  const result = await pool.query(sql, [userId, id]);
  return result[0];
}

const review = async (score, comment, checkbox, userId, id) => {
  const sql = `
  INSERT INTO mydb.reviews
  (score, comment, checkbox, users_id, photo_id)
  VALUES (?, ?, ?, ?, ?);

  `;
  const result = await pool.query(sql, [score, comment, checkbox, userId, id]);
  return result;
}

const reviewFromUser = async (id, userId) => {
  const sql = `
  select score, comment, checkbox 
  from reviews
  where photo_id = ?
  and users_id = ?;
  `;
  const result = await pool.query(sql, [id, userId]);
  return result[0];
}
const getReviews = async (photoId) => {
  const sql = `
  select u.id as user_id, u.username, u.first_name, u.last_name, u.avatar, p.id as photo_id, r.score, r.comment, r.id
  from reviews r
  join photos p
  on p.id = r.photo_id
  join users u
  on u.id = r.users_id
  where r.photo_id = ${photoId};
  `;

  const result = await pool.query(sql);
  return result;
}

export default {
  getAll,
  getUsersAll,
  searchBy,
  checkIfReviewed,
  review,
  reviewFromUser,
  getReviews
}
