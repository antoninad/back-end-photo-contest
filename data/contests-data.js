/* eslint-disable max-len */
/* eslint-disable consistent-return */
import pool from './pool.js';

const getPh = async (phaseId) => {
  const sql = `
  select c.id as contestId, c.title, cat.category,
  t.type, p.name as phase, u.username,
  ph1_limit_days as ph1Days, ph2_limit_hours as ph2Hours, image_url as imageUrl
  from contests c
  join categories cat
  on cat.id = c.categories_id
  join types t
  on t.id = c.types_id
  join users u
  on u.id =  c.users_id
  join phase p
  on p.id = c.phase_id
  where c.phase_id = ?;
  `;
  const result = await pool.query(sql, [phaseId]);
  return result;
}

const getPrevious = async (userId) => {
  const sql = `
  select con.id as contestId,
  con.title, cat.category,
  t.type, con.users_id as creatorId,
  ph.name as phase, con.ph1_limit_days as ph1LimitDays,
  con.ph2_limit_hours as ph2LimitHours,
  u.username as creator,
  con.image_url as imageUrl
  from users_has_contest uhc
  join contests con
  on con.id = uhc.contest_id
  join categories cat
  on cat.id = con.categories_id
  join types t
  on t.id = con.types_id
  join phase ph
  on ph.id = con.phase_id
  join users u
  on u.id = con.users_id
  where uhc.users_id = ?
  and phase_id = 3;
  `;
  const result = await pool.query(sql, [userId]);
  return result;
}

const getBy = async (column, name) => {
  const sql = `
  select * from contests
  where ${column} = ?;
  `
  const result = await pool.query(sql, [name]);
  return result[0];
}

const create =
  async (title, category, typeId,
      ph1LimitDays, ph2LimitHours, userId, imageUrl) => {
    const insertCategoryIfNoSuchExist = `
    INSERT INTO categories(category)
    SELECT * FROM (SELECT '${category}') AS tmp
    WHERE NOT EXISTS (
      SELECT category FROM categories WHERE category = '${category}'
    ) LIMIT 1;
    `;

    await pool.query(insertCategoryIfNoSuchExist, [category]);

    const idQuery = await pool.query(`
      SELECT id
      FROM categories
      WHERE category = '${category}';
    `);

    const categoryId = idQuery[0].id;

    const sql = `
    INSERT INTO contests
    (title, categories_id, types_id,
      ph1_limit_days, ph2_limit_hours, users_id, image_url)
    VALUES (?,?,?,?,?,?,?);
    `;

    const result =
      await pool.query(sql,
          [title, categoryId, typeId,
            ph1LimitDays, ph2LimitHours, userId, imageUrl]);

    const getInfo = `
    SELECT * FROM contests
    where id=?
    `
    const info = await pool.query(getInfo, [result.insertId]);

    const jobNextPhase2 = `
    CREATE EVENT job_contest_ph2_?
    ON SCHEDULE AT ?
    DO UPDATE contests SET phase_id = 2 WHERE (id = ?);
    `

    const jobNextPhase3 = `
     CREATE EVENT job_contest_ph3_?
     ON SCHEDULE AT ?
     DO UPDATE contests SET phase_id = 3 WHERE (id = ?);
    `

    await pool.query(jobNextPhase2, [info[0].id, `${info[0].ph1_limit_days}T00:00`, info[0].id]);
    await pool.query(jobNextPhase3, [info[0].id, info[0].ph2_limit_hours, info[0].id]);

    return result;
  };

const searchBy = async (column, value) => {
  const sql = `
  select c.id, c.title, cat.category, t.type,
  c.ph1_limit_days, c.ph2_limit_hours, c.phase_id, c.users_id, c.is_active
  from contests c
  join categories cat
  on cat.id = c.categories_id
  join types t
  on t.id = c.types_id
  where c.${column} like '%${value}%';
  `;
  const result = await pool.query(sql);
  // eslint-disable-next-line consistent-return
  const res = result.map((contest) => {
    if (contest.id) {
      return contest;
    }
  })
  return res;
};

const getOpen = async () => {
  const sql = `
  select con.id as contestId, con.title, cat.category, ph.name as phase,
  con.ph1_limit_days as ph1LimitDays, con.ph2_limit_hours as ph2LimitHours,
  u.username as creator, image_url as imageUrl
  from contests con
  join categories cat
  on cat.id = categories_id
  join phase ph
  on ph.id = con.phase_id
  join users u
  on u.id = con.users_id
  where con.is_active = 1
  and  con.types_id = 1
  and con.phase_id = 1;
  `;
  // is_active = 0 is for non-active contest
  const result = await pool.query(sql);
  const allOpen = [];
  for (const con of result) {
    allOpen.push(con);
  }
  return allOpen;
};

const getInvitational = async (userId) => {
  const sql = `
  select con.id as contestId, con.title, cat.category, ph.name as phase,
  con.ph1_limit_days as ph1LimitDays, con.ph2_limit_hours as ph2LimitHours,
  u.username as creator, image_url as imageUrl
  from users_are_invited uai
  join contests con
  on con.id = uai.contest_id
  join categories cat
  on cat.id = con.categories_id
  join phase ph
  on ph.id = con.phase_id
  join users u
  on u.id = con.users_id
  where uai.users_id = ?
  and con.is_active = 1;
  `;
  const result = await pool.query(sql, [userId]);
  const contests = [];
  for (const contest of result) {
    contests.push(contest);
  }
  return contests;
}

const getCurr = async (userId) => {
  const sql = `
  select c.id as contestId, c.title, cat.category,
  ph.name as phase, c.ph1_limit_days as ph1LimitDays,
  c.ph2_limit_hours as ph2LimitHours,
  u.username as creator, c.image_url as imageUrl
  from users_has_contest uhc
  join users u
  on u.id = uhc.users_id
  join contests c
  on c.id = uhc.contest_id
  join categories cat
  on c.categories_id = cat.id
  join phase ph
  on ph.id = c.phase_id
  where uhc.users_id = ?
  and c.is_active = 1
  and c.phase_id != 3;
  `;
  const result = await pool.query(sql, [userId]);
  const res = result.map((contest) => {
    if (contest.contestId) {
      return contest;
    }
  })
  return res;
};

const getPhotos = async (value) => {
  const sql = `
  select * from photos
  where contest_id = ?;
  `;
  const result = await pool.query(sql, [value]);

  const avgScore = `
  select ph.id as photoId, ph.contest_id as contestId,
  ph.image_url as imageUrl, ph.title, ph.story,
  u.username, u.avatar,
  AVG(r.score) as avgScore
  from photos ph
  join reviews r
  on r.photo_id = ph.id
  join users u
  on ph.users_id = u.id
  where ph.id = ?;
  `;

  const photos = [];
  for (const photo of result) {
    const score = await pool.query(avgScore, [photo.id]);

    if (!score[0].avgScore) {
      photos.push({ ...score[0], avgScore: 3 });
    } else {
      photos.push(score[0]);
    }
  }
  return photos;
};

const getReview = async (contestId, photoId) => {
  const sql = `
  select p.contest_id, p.id as photo_id, p.image_url,
  p.title,  r.score, r.comment
  from reviews r
  join photos p
  on p.id = r.photo_id
  where p.contest_id = ${contestId}
  and r.photo_id = ${photoId};
  `;
  const result = await pool.query(sql);
  const res = result.map((contest) => {
    if (contest.contest_id) {
      return contest;
    }
  })
  return res;
};

const getIndividual = async (value) => {
  const sql = `
  select c.id as contestId, c.title, cat.category, 
  c.ph1_limit_days as ph1LimitDays, c.ph2_limit_hours as ph2LimitHours,
  c.phase_id as phaseId
  from contests c
  join categories cat
  on c.categories_id = cat.id
  where c.id = ?;
  `;
  const result = await pool.query(sql, [value]);
  const res = result.map((contest) => {
    if (contest.contestId) {
      return contest;
    }
  })
  return res;
};

const getIndividualFullInfo = async (value) => {
  const sql = `
  select *
    from contests
    where id = ${value};

  `;
  const result = await pool.query(sql);
  const res = result.map((contest) => {
    if (contest.id) {
      return contest;
    }
  })

  return res;
};

const getPhotosInContest = async (contestId) => {
  const sql = `
  select p.image_url, p.title, p.story,
  u.username as photographer, u.first_name,
  u.last_name, r.score, r.comment, r.checkbox,
  r.users_id as reviewer 
  from photos p
  join reviews r
  on r.photo_id = p.id
  join users u
  on u.id = p.users_id
  where p.contest_id = ?;
  `;
  const result = await pool.query(sql, [contestId]);
  const photos = [];
  for (const photo of result) {
    photos.push(photo);
  }
  return photos;
}

const nextContestId = async () => {
  const sql = `
  SELECT MAX(con.id) as nextId
  FROM contests con;
  `;
  const result = await pool.query(sql);
  return result[0];
}

const checkIfUploaded = async (userId, contestId) => {
  const sql = `
    select * 
    from photos
    where users_id = ${userId}
    and contest_id = ${contestId}
  `;

  const result = await pool.query(sql);
  return result[0];
}

const upload = async (imageUrl, title, story, userId, contestId) => {
  const sql = `
    INSERT INTO photos (image_url, title, story, users_id, contest_id) 
    VALUES (?, ?, ?, ?, ?);
  `;

  const sql1 = `
    UPDATE users_has_contest
    SET has_uploaded = 1 
    WHERE users_id = ${userId}
    AND contest_id = ${contestId};
  `;

  const result = await pool.query(sql, [imageUrl, title, story, userId, contestId]);
  const result1 = await pool.query(sql1);

  return (result);
}

const joinContest = async (userId, contestId) => {
  const sql = `
    INSERT INTO users_has_contest (users_id, contest_id) 
    VALUES (?, ?);
  `;

  const result = await pool.query(sql, [userId, contestId]);
  return result;
};

const addPointsWhenJoin = async (userId) => {
  const sql = `
    UPDATE users u
    SET curr_points = curr_points + 1
    WHERE u.id = ${userId} ;
  `;

  const result = await pool.query(sql);
  return result;
};

const addPointsIfInvited = async (userId) => {
  const sql = `
    UPDATE users u
    SET curr_points = curr_points + 3
    WHERE u.id = ${userId} ;
  `;

  const result = await pool.query(sql);
  return result;
};

const ifIsInvited = async (contestId, userId) => {
  const sql = `
    select * from users_are_invited
    where users_id = ?
    and contest_id = ?;
  `;

  const result = await pool.query(sql, [userId, contestId]);
  return result[0];
};

const ifIsJoined = async (userId, contestId) => {
  const sql = `
    select * from users_has_contest
    where users_id = ${userId}
    and contest_id = ${contestId};
  `;

  const result = await pool.query(sql);
  return result[0];
};

const getInvitedAsJury = async (userId) => {
  const sql = `
  select con.id as contestId, con.title, cat.category, t.type, ph.name as phase, u.username, 
  con.ph1_limit_days as ph1Days, con.ph2_limit_hours as ph2Hours,
  image_url as imageUrl
  from users_are_jury uaj
  join contests con
  on con.id = uaj.contests_id
  join categories cat
  on cat.id = con.categories_id
  join phase ph
  on ph.id = con.phase_id
  join users u
  on u.id = con.users_id
  join types t
  on con.types_id = t.id
  where uaj.users_id = ?
  and con.is_active = 1;
  `;
  const result = await pool.query(sql, [userId]);
  const contests = [];
  for (const contest of result) {
    contests.push(contest);
  }
  return contests;
}

const getWinnerPhotos = async (contestId) => {
  const sql = `
  select * from photos
  where contest_id = ?;
  `;
  const result = await pool.query(sql, [contestId]);

  const avgScore = `
  select ph.id as photoId, u.id as userId, u.username, u.avatar, ph.title, ph.story,
  AVG(r.score) as avgScore, ph.image_url as imageUrl
  from photos ph
  join reviews r
  on r.photo_id = ph.id
  join users u
  on ph.users_id = u.id
  where ph.id = ?;
  `;

  const photos = [];
  for (const photo of result) {
    const score = await pool.query(avgScore, [photo.id]);

    if (!score[0].avgScore) {
      photos.push({ ...score[0], avgScore: 3 });
    } else {
      photos.push(score[0]);
    }
  }
  const ordered = photos.sort((a, b) => b.avgScore - a.avgScore);
  const winners = [];

  for (const ph of ordered) {
    if (winners.length < 3) {
      winners.push(ph);
    }
  }

  return winners;
}
export default {
  getPh,
  getPrevious,
  getBy,
  create,
  searchBy,
  getOpen,
  getInvitational,
  getCurr,
  getPhotos,
  getReview,
  getIndividual,
  getIndividualFullInfo,
  getPhotosInContest,
  nextContestId,
  checkIfUploaded,
  upload,
  joinContest,
  addPointsWhenJoin,
  addPointsIfInvited,
  ifIsInvited,
  ifIsJoined,
  getInvitedAsJury,
  getWinnerPhotos,
}
