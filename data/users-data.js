/* eslint-disable max-len */
import pool from './pool.js';

const getSignInInfo = async (username) => {
  const sql = `
    select u.id, u.username, u.password, r.type as role, u.avatar, ra.type as rank,
    u.first_name as firstName, u.last_name as lastName
    from users u
    join roles r
    on r.id = u.roles_id
    join ranks ra
    on ra.id = u.ranks_id
    where u.username = ?;
  `;
  const result = await pool.query(sql, [username]);
  return result[0];
};

const getRole = async (id) => {
  const sql = `
  select u.id, r.type as role
  from users u
  join roles r
  on r.id = u.roles_id
  where u.id = ${id};
  `;
  const result = await pool.query(sql);
  return result[0];
};

const createUser = async (username, password, firstName, lastName) => {
  const sql = `
  INSERT INTO users (username, password, first_name, last_name)
  VALUES (?, ?, ?, ?);
  `;
  const result = await pool.query(sql, [username, password, firstName, lastName]);
  return {
    id: result.insertId,
    username: username,
    firstName: firstName,
    lastName: lastName
  };
}

const getToken = async (token) => {
  const sql = `SELECT *
  FROM token_blacklist 
  WHERE token = ?;`

  return pool.query(sql, [token]);
}

const addToken = async (token) => {
  const sql = ` INSERT INTO token_blacklist(token)
  VALUES (?)`

  return pool.query(sql, [token]);
}

const getOrderedByRank = async () => {
  const sql = `
  select u.id as user_id, u.username, u.first_name, u.last_name, u.curr_points, r.type as rank, u.avatar 
  from users u
  join ranks r
  on r.id = u.ranks_id
  where u.roles_id = 1
  order by u.curr_points desc;
  `;
  const result = await pool.query(sql);
  return result;
}

const getPointsAndRanking = async (userId, username) => {
  const sql = `
  select u.id, u.username, u.curr_points as currPoints, r.type,
  u.next_rank_in as leftPoints
  from users u
  join ranks r
  on r.id = u.ranks_id
  where u.id = ?
  or u.username = ?;
  `;
  const result = await pool.query(sql, [userId, username]);
  return result[0];
}

const addAsJury = async (candidateId, contestId) => {
  const sql = `
  INSERT INTO users_are_jury (users_id, contests_id)
  VALUES (?, ?);
  `;
  const result = await pool.query(sql, [candidateId, contestId]);
  return result;
}

const addAsInvited = async (userId, contestId) => {
  const sql = `
  INSERT INTO users_are_invited (contest_id, users_id) 
  VALUES (?, ?);
  `;
  const result = await pool.query(sql, [contestId, userId]);
  return result;
}

const updateAvatar = async (id, path) => {
  const sql = `
  UPDATE users SET
  avatar = ?
  WHERE id = ?
  `;

  return await pool.query(sql, [path, id]);
}

const getAll = async () => {
  const sql = `
  select username from users
  where roles_id = 1;
  `;
  const users = [];
  const result = await pool.query(sql);
  for (const user of result) {
    users.push(user.username);
  }
  return users;
}

const getMasterRanked = async () => {
  const sql = `
  select username from users
  where ranks_id = 3;
  `;
  const users = [];
  const result = await pool.query(sql);
  for (const user of result) {
    users.push(user.username);
  }
  return users;
}

const getByRank = async (value) => {
  const sql = `
  select u.id, u.username, u.first_name, u. last_name, u.curr_points, u.ranks_id, r.type as rank
  from users u
  join ranks r
  on r.id = u.ranks_id
  where u.ranks_id = ${value};
  `;
  const result = await pool.query(sql);
  return result[0];
};

const isJury = async (contestId, userId) => {
  const sql = `
  select *
  from users_are_jury uaj
  where contests_id = ?
  and users_id = ?;
  `;
  const result = await pool.query(sql, [contestId, userId]);
  return result[0];
};

const getAvatar = async (userId) => {
  const sql = `
    SELECT u.id, u.username, u.avatar
    FROM users u
    WHERE u.id = ?;
  `;
  const result = await pool.query(sql, [userId]);
  return result[0];
};

export default {
  getSignInInfo,
  createUser,
  getToken,
  addToken,
  getOrderedByRank,
  getPointsAndRanking,
  addAsJury,
  addAsInvited,
  updateAvatar,
  getAll,
  getMasterRanked,
  getRole,
  getByRank,
  isJury,
  getAvatar
}
