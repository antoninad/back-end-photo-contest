/* eslint-disable max-len */
import express from 'express';
import contestService from '../services/contests-service.js';
import userService from '../services/users-service.js';
import contestData from '../data/contests-data.js';
import userData from '../data/users-data.js';
import serviceErrors from '../services/error-service.js';
import { authMiddleware, roleMiddleware, tokenMiddleware }
  from '../auth/auth-middleware.js';
import multer from 'multer';
import storage from '../storage.js';

// eslint-disable-next-line new-cap
const contestsController = express.Router();

contestsController
// JOIN TO CONTEST
    .put('/:id',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res,) => {
          const { id } = req.params;
          const userId = req.user.id;

          const { error } = await contestService.joinToContest(contestData, userData)(userId, id);
          if (error) {
            return res.status(400).send({ error, message: 'You cant be joined to the contest!' });
          }
          return res.status(200).send({ message: `Successful join to contest ${id}` });
        })

    .get('/:id/joinavailable',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res,) => {
          const { id } = req.params;
          const userId = req.user.id;
          const { result } = await contestService.canUserJoinToContest(contestData, userData)(userId, +id);

          return res.status(200).send(result);
        })

    .get('/:id/uploadCheck',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res,) => {
          const { id } = req.params;
          const userId = req.user.id;
          const { result } = await contestService.canUserUploadPhoto(contestData)(userId, +id);

          return res.status(200).send(result);
        })

// VIEW CONTESTS IN CERTAIN PHASE (1, 2, 3)
    .get('/phase/:id',
        authMiddleware,
        roleMiddleware('Organizator'),
        tokenMiddleware(),
        async (req, res) => {
          // phase 1 id = 1
          // phase 2 id = 2
          // phase 3 id = 3
          const { id } = req.params;
          const { error, contests } =
        await contestService.getPhase(contestData)(id);
          if (error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(404).send({
              message: 'Contests in Phase I are not found!'
            });
          }
          if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
            return res.status(404).send({
              message: 'There is no such phase.'
            })
          }
          return res.status(200).send(contests);
        })

// VIEW PREV CONTESTS
    .get('/finished',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res) => {
          const userId = req.user.id;
          const { error, contests } =
        await contestService.getPrevContests(contestData)(userId);
          if (error) {
            return res.status(404).send({
              message: 'Contests in Phase III are not found!'
            });
          }
          return res.status(200).send(contests);
        })

// CREATE A CONTEST
    .post('/setup',
        authMiddleware,
        roleMiddleware('Organizator'),
        tokenMiddleware(),
        multer({ storage: storage }).single('uploadPhoto'),
        // createValidator(createContestSchema),
        async (req, res) => {
          const { title, category, typeId, ph1LimitDays, ph2LimitHours, selectJury, invitedUsers } = req.body;
          const userId = req.user.id;

          if (req.file) {
            const { error, newContest } =
            await contestService.createContest(contestData, userData)(
                title,
                category,
                +typeId,
                ph1LimitDays,
                ph2LimitHours,
                selectJury,
                invitedUsers,
                userId,
                req.file.filename
            );

            if (error) {
              return res.status(409).send({
                error: error
              });
            }
          }

          return res.status(200).send({
            message: 'New contest was created seccessfully.'
          });
        }
    )

// VIEW AN OPEN CONTESTS
    .get('/open',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res) => {
          const userId = req.user.id;
          const contests =
        await contestService.getOpenContests(contestData)(userId);
          return res.status(200).send(contests);
        })

// VIEW CONTESTS IN WHICH IS INVITED
    .get('/invitational',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res) => {
          const userId = req.user.id;
          const { error, contests } =
        await contestService.getInvitaionalContests(contestData)(userId);

          if (error) {
            return res.status(404).send({
              message: 'You have no invitations!'
            });
          }
          return res.status(200).send(contests);
        })

// VIEW CURR CONTESTS
    .get('/current',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res) => {
          const userId = req.user.id;
          const { error, contests } =
        await contestService.getCurrContests(contestData)(userId);
          if (error) {
            return res.status(404).send({
              message: 'You have not participated in this contest!'
            });
          }
          return res.status(200).send(contests);
        })

// VIEW ALL PHOTOS FOR A CONTEST WITH :id
    .get('/:id/photos',
        authMiddleware,
        tokenMiddleware(),
        async (req, res) => {
          const { id } = req.params;
          const { error, photos } =
        await contestService.getPhotosForContest(contestData)(id);
          if (error) {
            return res.status(404).send({
              message: 'There are no photos for this contest!'
            });
          }
          return res.status(200).send(photos);
          // eslint-disable-next-line indent
    })

// VIEW A REVIEW
    .get('/:id/photos/:photoId',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res) => {
          const { id, photoId } = req.params;
          const { error, reviews } =
        await contestService.getReviewForPhoto(contestData)(id, photoId);
          if (error) {
            return res.status(404).send({
              message: 'There are no reviews for this photo!'
            });
          }
          return res.status(200).send(reviews);
        })

// VIEW INDIVIDUAL CONTEST WITH :id
    .get('/:id',
        authMiddleware,
        tokenMiddleware(),
        async (req, res) => {
          const { id } = req.params;
          const userId = req.user.id
          const { error, contest } =
        await contestService.getIndividualContest(contestData, userData)(id, userId);
          if (error) {
            return res.status(404).send({
              message: 'There is not such contest!'
            });
          }
          return res.status(200).send(contest);
        })

// VIEW ALL SUBMITTED PHOTOS IN THE CONTEST WITH :id
    .get('/:id/submittedPhotos',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res) => {
          const { id } = req.params;
          const { error, photos } =
        await contestService.getSubmittedPhotos(contestData)(id);

          if (error) {
            return res.status(404).send({
              message: 'There are no submitted photos for this contests!'
            });
          }
          return res.status(200).send(photos);
        })

// UPLOAD A PHOTO
    .post('/:contestId/uploadPhoto',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        // createValidator(uploadPhotoValidator),
        multer({ storage: storage }).single('uploadPhoto'),
        async (req, res,) => {
          const { title, story } = req.body;
          const { contestId } = req.params;
          const userId = req.user.id;

          if (title && (title.length < 2 || title.length > 15)) {
            return res.status(404).send({
              error: 'Title should be between [2...15] symbols!'
            });
          }

          if (story && (story.length < 15 || story.length > 100)) {
            return res.status(404).send({
              error: 'Story should be between [15...100] symbols!'
            });
          }

          if (!req.file) {
            return res.status(404).send({
              error: 'Plase upload a photo!'
            });
          }

          if (req.file) {
            const { error } = await contestService.uploadPhoto(contestData)(req.file.filename, title, story, userId, contestId);
            if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
              return res.sendStatus(500).send({
                error: 'Server error occurred. Our developers are working hard to resolve it.'
              });
            } else if (error === serviceErrors.DUPLICATE_RECORD) {
              return res.status(404).send({
                error: 'You already uploaded a photo!'
              });
            } else if (error === serviceErrors.RECORD_NOT_FOUND) {
              return res.status(404).send({
                error: 'There is not such contest existing!'
              });
            }

            return res.status(200).send({
              path: req.file.filename
            });
          }
          return res.status(404).send({
            message: 'Photo is not uploaded!'
          });
        })

// Checks if junkie is jury
    .get('/:id/isJury',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res) => {
          const contestId = +req.params.id;
          const userId = req.user.id;

          const { error, contests } = await userService.checkIfJury(userData)(contestId, userId);

          if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ error: 'Can not find any data!' });
          } else {
            res.status(200).send(contests);
          }
        }
    )

// Shows winner photos
    .get('/:id/winners',
        authMiddleware,
        tokenMiddleware(),
        async (req, res) => {
          const contestId = +req.params.id;

          const { error, photos } = await contestService.winnerPhotos(contestData)(contestId);

          return res.status(200).send(photos);
        }
    )
export default contestsController;
