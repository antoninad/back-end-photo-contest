/* eslint-disable max-len */
import express from 'express';
import { authMiddleware, roleMiddleware, tokenMiddleware } from '../auth/auth-middleware.js';
import userService from '../services/users-service.js';
import userData from '../data/users-data.js';
import contestData from '../data/contests-data.js';
import multer from 'multer';
import storage from '../storage.js';
import errorService from '../services/error-service.js';
import rankingPoints from '../constants/rankingPoints.js';

// eslint-disable-next-line new-cap
const usersController = express.Router();

export default usersController;

usersController
    .get('/all',
        authMiddleware,
        roleMiddleware('Organizator'),
        tokenMiddleware(),
        async (req, res) => {
          const { error, users } = await userService.getAllUsers(userData)();
          if (error) {
            return res.status(404).send({
              message: 'Users are not found!'
            });
          }
          return res.status(200).send(users);
        }
    )
    .get('/withMasterRank',
        authMiddleware,
        roleMiddleware('Organizator'),
        tokenMiddleware(),
        async (req, res) => {
          const { error, users } = await userService.getWithMasterRank(userData)();
          if (error) {
            return res.status(404).send({
              message: 'Users are not found!'
            });
          }
          return res.status(200).send(users);
        }
    )
    .get('/descRanking',
        authMiddleware,
        roleMiddleware('Organizator'),
        tokenMiddleware(),
        async (req, res) => {
          const { error, users } =
        await userService.getByRank(userData)();
          if (error) {
            return res.status(404).send({
              message: 'Users are not found!'
            });
          }
          return res.status(200).send(users);
        })

    .get('/scoringInfo',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res) => {
          const userId = req.user.id;
          const { error, info } =
        await userService.getScoringInfo(userData)(userId);
          if (error) {
            return res.status(404).send({
              message: 'User is not found!'
            });
          }
          return res.status(200).send(info);
        }
    )
// upload avatar
    .post('/avatar',
        authMiddleware,
        multer({ storage: storage }).single('avatar'),
        async (req, res,) => {
          const userId = req.user.id;

          const { error } = await userService.updateAvatar(userData)(
              userId,
              req.file.filename
          );

          if (error) {
            res.sendStatus(500);
          } else {
            res.status(200).send({
              path: req.file.filename
            });
          }
        })

// Get user by rankId
    .get('/ranks/:rankId',
        authMiddleware,
        // roleMiddleware('Junkie'),
        async (req, res) => {
          const { rankId } = req.params;

          const { error, user } = await userService.getUserByRank(userData)(+rankId);

          if (error === errorService.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'User with this rank is not found!' });
          } else {
            res.status(200).send(user);
          }
        })
// Contests, in which junkie is invited as Jury
    .get('/invitedAsJury',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res) => {
          const userId = req.user.id;

          const { error, contests } = await userService.getInvitedAsJuryContests(contestData)(userId);

          if (error === errorService.RECORD_NOT_FOUND) {
            res.status(404).send({ error: 'Can not find any data!' });
          } else {
            res.status(200).send(contests);
          }
        }
    )

// Get left points until next rank
    .get('/leftPoints',
        authMiddleware,
        roleMiddleware('Junkie'),
        async (req, res) => {
          const userId = req.user.id;

          const { pointsLeft } = await userService.getUntilNextRank(rankingPoints, userData)(+userId);
          res.status(200).send({ pointsLeft });
        })

// Get user's avatar
    .get('/avatar',
        authMiddleware,
        async (req, res) => {
          const userId = req.user.id;

          const { currAvatar } = await userService.getAvatarName(userData)(+userId);
          res.status(200).send(currAvatar);
        })
