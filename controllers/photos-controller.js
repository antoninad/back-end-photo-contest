/* eslint-disable max-len */
import express from 'express';
import photoService from '../services/photos-service.js';
import photoData from '../data/photos-data.js';
import { authMiddleware, roleMiddleware, tokenMiddleware } from '../auth/auth-middleware.js';
import { createReview, createValidator } from '../validations/index.js';

// eslint-disable-next-line new-cap
const photosController = express.Router();

photosController

    // VIEW ALL PHOTOS
    .get('/',
        authMiddleware,
        tokenMiddleware(),
        async (req, res) => {
          const { title } = req.query;
          const photos = await photoService.getAllPhotos(photoData)(title);
          res.status(200).send(photos);
        })

    // VIEW ALL USER'S PHOTOS
    .get('/photoHistory',
        authMiddleware,
        roleMiddleware('Junkie'),
        tokenMiddleware(),
        async (req, res) => {
          const userId = req.user.id;
          const photos = await photoService.getAllUsersPhotos(photoData)(userId);
          res.status(200).send(photos);
        })

    // CREATE A REVIEW FOR A PHOTO WITH :id
    .post('/:id/review',
        authMiddleware,
        tokenMiddleware(),
        // createValidator(createReview),
        async (req, res) => {
          const { id } = req.params;
          const data = req.body;
          const userId = req.user.id;

          const { error, review } =
          await photoService.createReview(photoData)(id, data, userId);

          if (error) {
            return res.status(404).send({
              error: 'You already reviewed this photo.'
            });
          }

          return res.status(200).send({
            message: 'Your review was submitted!'
          });
        })
    // CHECK IF THE USER HAS ALREADY WROTE REVIEW
    .get('/:id/allReviews',
        authMiddleware,
        tokenMiddleware(),
        async (req, res) => {
          const { id } = req.params;
          const userId = req.user.id;

          const { error, reviews, photoId } = await photoService.checkIfHasWroteReview(photoData)(id, userId);

          if (error) {
            return res.status(404).send({
              error: 'Error occured.'
            });
          }

          return res.status(200).send({
            message: reviews, photoId
          });
        }
    )

    // VIEW ALL PHOTO REVIEWS
    .get('/:id/reviews',
        authMiddleware,
        tokenMiddleware(),
        async (req, res) => {
          const { id } = req.params;
          const reviews = await photoService.getAllPhotoReviews(photoData)(id);
          res.status(200).send(reviews);
        })


export default photosController;
