/* eslint-disable max-len */
import express from 'express';
import createToken from '../auth/create-token.js';
import { authMiddleware, tokenMiddleware } from '../auth/auth-middleware.js';
import userService from '../services/users-service.js';
import userData from '../data/users-data.js';
import { createUserSchema, createValidator, signInUserSchema } from '../validations/index.js';

// eslint-disable-next-line new-cap
const authController = express.Router();

authController

// SIGN IN
    .post('/signin', createValidator(signInUserSchema), async (req, res) => {
      const { username, password } = req.body;
      const { error, user } =
      await userService.signIn(userData)(username, password);

      if (error) {
        return res.status(404).send({
          message: 'Invalid username/password'
        })
      }

      const payload = {
        sub: user.id,
        username: user.username,
        role: user.role,
        firstName: user.firstName,
        lastName: user.lastName,
        avatar: user.avatar,
        rank: user.rank
      };

      const token = createToken(payload);
      return res.status(200).send({
        token: token
      });
    })

// SIGN OUT
    .delete('/signout', authMiddleware, tokenMiddleware(),
        async (req, res) => {
          const token = req.headers.authorization;
          const newToken =
        await userService.addTokenToBlackList(userData)(token);

          res.status(200).send({ message: `You just signed out.` });
        })

// SIGN UP
    .post('/signup', createValidator(createUserSchema), async (req, res) => {
      const createData = req.body;
      const { error, user } = await userService.signUp(userData)(createData);
      if (error) {
        return res.status(404).send({ message: 'Username is not available.' });
      }
      return res.status(201).send(user);
    });

export default authController;
