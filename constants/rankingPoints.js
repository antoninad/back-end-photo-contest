const rankingPoints = [
  {
    type: 'Junkie',
    minPoints: 0,
    maxPoints: 50
  },
  {
    type: 'Enthusiast',
    minPoints: 51,
    maxPoints: 150
  },
  {
    type: 'Master',
    minPoints: 151,
    maxPoints: 1000
  },
  {
    type: 'Wise and Benevolent Photo Dictator',
    minPoints: 1001,
    maxPoints: Number.MAX_SAFE_INTEGER
  },
]

export default rankingPoints;
