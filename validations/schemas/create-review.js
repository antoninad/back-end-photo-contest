export const createReview = {
  score: (value) => {
    if (!value || typeof value !== 'number' || value < 1 || value > 10) {
      return 'Score points should be a number in interval [1, 10]!';
    }

    return null;
  },
  comment: (value) => {
    if (!value || typeof value !== 'string' ||
    value.length < 25 || value.length > 100) {
      return 'Comment should be a long text with [25, 100] symbols';
    }

    return null;
  },
  checkbox: (value) => {
    if (value === 0 || value === 1) {
      return null;
    }
    return 'Checkbox can be either 0 (does not fit) or 1 (does fit).'
  }
}

