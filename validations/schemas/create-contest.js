/* eslint-disable max-len */
export const createContestSchema = {
  title: (value) => {
    if (!value) {
      return 'title is required!';
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
      return 'title should be a string in range [3...25]';
    }
    return null;
  },
  category: (value) => {
    if (!value) {
      return 'category is required';
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
      return 'category should be a string in range [3...25]';
    }

    return null;
  },
  typeId: (value) => {
    if (!value) {
      return 'Type is required';
    }

    if (typeof value !== 'number' || value < 1|| +value > 2) {
      return 'Type should be a number, 2  for Invitational, 1 for Open';
    }

    return null;
  },
  ph1LimitDays: (value) => {
    if (!value) {
      return 'ph1LimitDays is required';
    }

    if (typeof value !== 'number' || value < 0 || value > 31) {
      return 'ph1LimitDays should be a number in diapason [1, 31] days';
    }

    return null;
  },
  ph2LimitHours: (value) => {
    if (!value) {
      return 'ph2LimitHours is required';
    }

    if (typeof value !== 'number' || value < 0 || value > 24) {
      return 'ph2LimitHours should be a number in diapason [1, 24] hours';
    }

    return null;
  },
  selectJury: (value) => {
    if (typeof value !== 'string') {
      return 'In order to select a jury: usernames must be comma-separated or leave empty string!';
    }

    return null;
  },
  invitedUsers: (value) => {
    if (typeof value !== 'string') {
      return 'In order to invite users: usernames must be comma-separated or leave empty string!';
    }

    return null;
  }
}
