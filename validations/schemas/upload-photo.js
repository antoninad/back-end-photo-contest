export const uploadPhotoValidator = {
  title: (value) => {
    if (!value || typeof value !== 'string' ||
          value.length < 2 || value.length > 50) {
      return 'Title should be a short text in the range [2...50] symbols';
    }

    return null;
  },

  story: (value) => {
    if (!value || typeof value !== 'string' ||
      value.length < 5 || value.length > 100) {
      return 'Story should be a long text in the range [5...100] symbols';
    }

    return null;
  },
}
