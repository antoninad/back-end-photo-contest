-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.5-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `mydb`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `mydb`;

--
-- Table structure for table `awards`
--

DROP TABLE IF EXISTS `awards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `awards` (
  `id` int(10) unsigned zerofill NOT NULL DEFAULT 0000000000,
  `name` varchar(45) DEFAULT NULL,
  `points` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `awards`
--

LOCK TABLES `awards` WRITE;
/*!40000 ALTER TABLE `awards` DISABLE KEYS */;
/*!40000 ALTER TABLE `awards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Nature'),(2,'Architecture'),(3,'Animals'),(4,'People'),(5,'Digital'),(6,'Winter'),(7,'Sport'),(8,'Night'),(9,'Spring'),(10,'Landscape'),(11,'Christmas'),(12,'Dogs'),(13,'Horses'),(14,'Dance'),(15,'Travel'),(16,'Couples'),(17,'Old people'),(18,'Black and white'),(19,'Ski'),(20,'Digital art');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contests`
--

DROP TABLE IF EXISTS `contests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `categories_id` int(11) NOT NULL,
  `types_id` int(11) NOT NULL,
  `ph1_limit_days` varchar(45) DEFAULT NULL,
  `ph2_limit_hours` varchar(45) DEFAULT NULL,
  `phase_id` int(11) NOT NULL DEFAULT 1,
  `users_id` int(11) NOT NULL,
  `is_active` tinyint(4) DEFAULT 1,
  `image_url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`),
  KEY `fk_contest_categories1_idx` (`categories_id`),
  KEY `fk_contest_types1_idx` (`types_id`),
  KEY `fk_contests_phase1_idx` (`phase_id`),
  KEY `fk_contests_users1_idx` (`users_id`),
  CONSTRAINT `fk_contest_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contest_types1` FOREIGN KEY (`types_id`) REFERENCES `types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contests_phase1` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contests_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contests`
--

LOCK TABLES `contests` WRITE;
/*!40000 ALTER TABLE `contests` DISABLE KEYS */;
INSERT INTO `contests` VALUES (1,'Autumn tones',1,1,'2020-12-02','2020-12-02T12:06',3,1,1,'1606860379715_483275555.jpg'),(2,'Modern Architecture',2,1,'2020-12-02','2020-12-02T13:29',3,2,1,'1606861642021_972199470.jpg'),(3,'Pets',3,1,'2020-12-03','2020-12-03T22:41',3,1,1,'1606902431993_869517581.jpg'),(4,'People in my town',4,1,'2020-12-05','2020-12-05T10:50',1,1,1,'1606902622562_504873283.jpg'),(5,'Digital Art',5,1,'2020-12-06','2020-12-06T22:55',3,2,1,'1606902732112_746571732.jpg'),(6,'Winter season',6,1,'2020-12-03','2020-12-03T12:20',2,2,1,'1606936920154_697945061.jpg'),(7,'Sporting',7,1,'2020-12-04','2020-12-04T22:23',3,1,1,'1606937071524_115882920.jpg'),(8,'Nighty night',8,1,'2020-12-05','2020-12-05T21:33',1,2,1,'1606937670637_479703853.jpg'),(9,'Seasonality',9,1,'2020-12-05','2020-12-05T19:35',1,1,1,'1606938013592_016277940.jpg'),(10,'Beautiful landscapes',10,1,'2020-12-24','2020-12-24T19:46',2,1,1,'1606938489820_271008686.jpg'),(11,'Very Merry Christmas ',11,1,'2020-12-05','2020-12-05T19:48',1,2,1,'1606938612208_701125675.jpg'),(12,'Doggies',12,1,'2020-12-05','2020-12-05T19:52',1,1,1,'1606938801818_613988521.jpg'),(13,'Horse riding',13,1,'2020-12-04','2020-12-04T19:55',1,2,1,'1606938984131_409503975.jpg'),(14,'Dancing stars',14,1,'2020-12-05','2020-12-05T19:58',2,2,1,'1606939130054_765523703.jpg'),(15,'Passion for travel',15,1,'2020-12-10','2020-12-10T19:59',2,1,1,'1606939236911_529438755.jpg'),(16,'Couple goals',16,2,'2020-12-05','2020-12-05T20:00',1,2,1,'1606939330137_542154095.jpg'),(17,'Old but gold',17,2,'2020-12-12','2020-12-12T20:02',2,1,1,'1606939400894_824567613.jpg'),(18,'Christmas spirit',11,1,'2020-12-05','2020-12-04T01:01',2,1,1,'1606946618376_168390230.jpg'),(19,'Black & White',18,2,'2020-12-05','2020-12-05T22:03',1,1,1,'1606946798027_260731740.jpg'),(20,'Skiing',19,2,'2020-12-05','2020-12-05T22:07',1,2,1,'1606947693090_537575312.jpg'),(21,'Digital Portraits',20,2,'2020-12-05','2020-12-05T22:29',1,1,1,'1606948339859_803232485.jpg');
/*!40000 ALTER TABLE `contests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phase`
--

DROP TABLE IF EXISTS `phase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `phase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phase`
--

LOCK TABLES `phase` WRITE;
/*!40000 ALTER TABLE `phase` DISABLE KEYS */;
INSERT INTO `phase` VALUES (1,'Phase I'),(2,'Phase II'),(3,'Final Phase');
/*!40000 ALTER TABLE `phase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(245) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `story` longtext DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_photo_users1_idx` (`users_id`),
  KEY `fk_photo_contest1_idx` (`contest_id`),
  CONSTRAINT `fk_photo_contest1` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_photo_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (1,'1606861842873_259076321.jpg','Autumn through my eyes','“The heat of autumn is different from the heat of summer. One ripens apples, the other turns them to cider.\" J.H.',3,1),(2,'1606898869608_220470609.jpg','Cloud Gate in Chicago','Cloud Gate is a public sculpture by Indian-born British artist Sir Anish Kapoor, that is the centerpiece of AT&T Plaza at Millennium Park in the Loop community area of Chicago, Illinois. The sculpture and AT&T Plaza are located on top of Park Grill, between the Chase Promenade and McCormick Tribune Plaza & Ice Rink. Constructed between 2004 and 2006, the sculpture is nicknamed The Bean because of its shape, a name Kapoor initially disliked, but later grew fond of. Made up of 168 stainless steel plates welded together, its highly polished exterior has no visible seams. It measures 33 by 66 by 42 feet (10 by 20 by 13 m), and weighs 110 short tons (100 t; 98 long tons).',3,2),(3,'1606899741025_847666220.jpg','Autumn leaves','Oh, fall! There are just so many reasons to love the season—where to even begin? Here, we\'ve rounded up a few of our very favorite fall quotes so you can remind yourself of the best that the season has to offer. From creating these fun fall crafts to picking out the perfect pumpkin for pumpkin decorating at one of these pumpkin farms, you and your family have plenty of ways to stay busy in the weeks ahead.',4,1),(4,'1606899868122_110188606.jpg','Significant building','Simplicity is another method that makes people take an interest in modern buildings. We have to agree that simple things are quite sophisticated, and architecture is no exception. However, whether it’s an art, clothes, or buildings, simplicity can never be bored, and it’s one of the features modern architecture puts focus on.',4,2),(5,'1606900945286_382687345.jpg','Bosco Verticale','The Vertical Wood is a spectacular skyscraper by Italian architect Stefano Boeri, which aspires to a complete redefinition of the concept of interaction between man and nature in an urban contest. Winner of the 2014 Highrise Award and the 14th Annual CTBUH International Best Tall Building Awards in 2015, this magnificent building is the perfect response to the conflicting needs of the modern city dweller. ',5,2),(6,'1606901046224_895618525.jpg','Autumn in Plovdiv','Go, sit upon the lofty hill, And turn your eyes around, Where waving woods and waters wild Do hymn an autumn sound. The summer sun is faint on them— The summer flowers depart— Sit still— as all transform’d to stone, Except your musing heart.',5,1),(7,'1606901317085_041437081.jpg','Autumn in Sofia','Autumn seemed to arrive suddenly that year. The morning of the first September was crisp and golden as an apple.',6,1),(8,'1606901619530_937746524.jpg','Modern and contemporary','Modern architecture boasts the actual structure and materials used in the building vs. covering them up with ornate designs. That is why most modern designs feature elements of wood, steel and glass, in order to show-off these industrial structural materials.',6,2),(9,'1606903129898_409825702.jpg','My best friend','If you don\'t own a dog, at least one, there may not necessarily be anything wrong with you, but there may be something wrong with your life.',6,3),(10,'1606903303048_279755540.jpg','Aftrernoon in Amsterdam','I love Amsterdam. The city is vibrant and alive. It’s fresh and so open. It’s definitely one of my favourite places.',6,4),(11,'1606903425003_205626187.jpg','Lady in white','We don’t ask a flower any special reason for its existence. We just look at it and are able to accept it as being something different for ourselves.',6,5),(13,'1606917672156_636864033.jpg','A fawn Pug puppy','Did you know that the Mop is one of the best - stand by the house. It has existed since 400 BC. Xp. It is no coincidence that people love these beautiful children. Bred in the past for the companions of the emperors, one day one of the most - the desired pets.',4,3),(14,'1606920126641_765386775.jpg','Krakow\'s Street Art','The districts of Kazimierz and Podgórze in Kraków host some of the most beautiful and distinct murals in Poland. Size is not their only impressive feature, they often have a more in-depth meaning people often miss upon first glance.',3,4),(15,'1606920301749_767523603.jpg','The Lady','She walks, on the streets, with a face that, doesn\'t belong.',3,5),(16,'1606920501260_254799026.jpg','The Pug Master','Viele Wege führen zum Mops, keiner an ihm vorbei!',4,5),(17,'1606920711833_283752298.jpg','Red Square','The tourists who stop by tend to be reverent, while the locals generally avoid the place altogether, sticking to the side streets. But for the last three weeks, the square has felt like the center of the world, or at least the center of the World Cup.',4,4),(18,'1606921041410_680830088.jpg','Grunty boy','If it looks like a rabbit, and it hops like a rabbit, run the other way and fast, then it is a rabbit. :))',5,3),(19,'1606921145270_207708929.jpg','Bulgarian singer','I like much of my digital art, not because of what it may seem to reveal to be my talent, but for the astonishing things my programs can do, which, unaided by them I could never have accomplished on my own.',5,5),(20,'1606921420465_086717000.jpg','Chill in Tridente','Rome’s Tridente is home to many of the city’s beloved sights, including the Spanish Steps, the Trevi Fountain and the magnificent churches of the Piazza del Popolo. ',5,4),(21,'1606935753843_807444212.jpg','Digital drawing','Digital drawing | Digital drawing | Digital drawing',7,5),(22,'1606940611424_010997538.jpg','Kids and dogs','Dogs and children can be great friends and having a dog can help children develop kindness, understanding and respect for living things. Dog companionship can improve a child¿s social skills with people and caring for a pet can encourage responsibility.  Because of this many families have dogs. Children quickly understand and learn to treat the dog as part of the family but it is important that parents teach children how to stay safe around dogs, to protect both child and dog. ',7,3),(23,'1606940680934_286794573.jpg','Merry Christmas','Christmas was traditionally a Christian festival celebrating the birth of Jesus, but in the early 20th century, it also became a secular family holiday, observed by Christians and non-Christians alike. The secular holiday is often devoid of Christian elements, with the mythical figure Santa Claus playing the pivotal role.',7,11),(24,'1606940912363_529813906.jpg','Winter season','comes from an old Germanic word that means “time of water” and refers to the rain and snow of winter in middle and high latitudes. In the Northern Hemisphere it is commonly regarded as extending from the winter solstice (year’s shortest day), December 21 or 22, to the vernal equinox (day and night equal in length), March 20 or 21, and in the Southern Hemisphere from June 21 or 22 to September 22 or 23. The low temperatures associated with winter occur only in middle and high latitudes; in equatorial regions, temperatures are almost uniformly high throughout the year. For physical causes of the seasons, see season.',7,6),(25,'1606941144280_313044489.jpg','Love sporting','Motivation is the foundation of all athletic effort and accomplishment. Without your desire and determination to improve your sports performances, all of the other mental factors, confidence, intensity, focus, and emotions, are meaningless.Oct 30, 2009',7,7),(26,'1606941236077_093767628.jpg','Night feels','The sky grew darker, painted blue on blue, one stroke at a time, into deeper and deeper shades of night.',7,8),(27,'1606941292766_750587452.jpg','Season change','“I wonder if the snow loves the trees and fields, that it kisses them so gently? And then it covers them up snug, you know, with a white quilt; and perhaps it says, \"Go to sleep, darlings, till the summer comes again.”',7,9),(28,'1606941412144_538922383.jpg','The beauty of nature','“If you truly love nature, you will find beauty everywhere.” \"Nature always wears the colors of the spirit.\" \"Look deep into nature, and then you will understand everything better.\"',7,10),(29,'1606941634544_368479067.jpg','My lovely doggo','My fashion philosophy is, if you\'re not covered in dog hair, your life is empty.',8,3),(30,'1606941675740_212548585.jpg','Lovely doggo','My fashion philosophy is, if you\'re not covered in dog hair, your life is empty.',8,3),(31,'1606941750051_155356206.jpg','Fav dog','My fashion philosophy is, if you\'re not covered in dog hair, your life is empty.',4,3),(32,'1606941820079_126905573.jpg','How not to adore?','In ancient times cats were worshipped as gods; they have not forgotten this.',4,3),(33,'1606941931492_924959740.jpg','Motivation','There may be people that have more talent than you, but theres no excuse for anyone to work harder than you do.',4,7),(34,'1606942892014_313780765.jpg','GOD backwards','I’ve seen a look in dogs’ eyes, a quickly vanishing look of amazed contempt, and I am convinced that basically dogs think humans are nuts.',4,3),(35,'1606943396558_016692983.jpg','Spring mood','But when fall comes, kicking summer out on its treacherous ass as it always does one day sometime after the midpoint of September, it stays awhile like an old friend that you have missed. It settles in the way an old friend will settle into your favorite chair and take out his pipe and light it and then fill the afternoon with stories of places he has been and things he has done since last he saw you.',4,9),(36,'1606943450269_197505879.jpg','Dream view','This is the view I want to see every day.',4,10),(37,'1606948603792_099435606.jpg','My two dogs','Dogs do speak, but only to those who know how to listen.',9,3),(38,'1606948983535_871631864.jpg','Smart doggo','Dogs are not our whole life, but they make our lives whole.',9,12),(39,'1606954011334_845706305.jpg','Love of a cat','Cats choose us, we don\'t own them. Time spent with cats is never wasted.',10,3),(40,'1606954284401_601088855.jpg','These two creatures <3','You know cats make amazing pets for plenty of reasons, but sometimes it can be hard to put into words exactly why they\'re so great. If you need a little help (or a caption for your next Instagram photo), scroll through these perfect quotes and sayings about cats that prove why they\'re such great creatures.',11,3),(41,'1606954411171_958632855.jpg','Happiness','Happiness is a warm puppy.',12,3),(42,'1606955462327_391309466.jpg','Winter Solstice','December 21 is the date of the winter’s solstice. For those who are located north of the equator, its the longest night of the year. ',4,6),(43,'1606955573747_197696983.jpg','Winter in Michigan','I really do find snow to be amazing. Not merely the way that it blankets the whole landscape and rests on the branches, but also the unique intricacy of each single snowflake. It takes a lot to move snow, and I have had my share of using the working end of a snow shovel. But the inconvenience of driving slower and moving snow has never eclipsed for me the beauty and wonder of a snowy Michigan winter.',6,6),(44,'1606955702541_986810660.jpg','Tenderness','The reality of the harshness of a winter gives a reset to the natural world around us. We have a few months without mosquitos, weeding, trimming and yellow jackets. Much of the outdoors takes a pause from human interaction, except for sledding, skiing, snowmobiling, ice-skating, hunting, ice-fishing, and pond hockey.',8,6),(45,'1606955884624_505963353.jpg','Winter in Bergen','Bergen has all the makings of an idyllic Nordic village: colorful wooden buildings, a scenic harbor, and sweeping views of the surrounding fjords. The town looks beautiful in the summer, sure, but it\'s during the winter months that you get to view the Northern Lights during their peak season. If you\'re going to be freezing up in Scandinavia, you might as well get to cross an astronomical wonder off of your bucket list.',5,6),(46,'1606955998281_430567540.jpg','Sunday walk','There\'s something magical about wintertime. But as the days start to get a little shorter and a lot colder, it\'s easy to lose sight of this sense of wonder. Fear not, all you need is a little shift in perspective to see the beauty of winter again. ',3,6),(47,'1606956122109_125411009.jpg','Winter is fun','Winter is not a season, it\'s a celebration!',9,6),(48,'1606956956096_557880067.jpg','Christmas at home','Christmas is the season of joy, of holiday greetings exchanged, of gift-giving, and of families united.',6,18),(49,'1606957024809_827679252.jpg','Sweetness in life','From home to home, and heart to heart, from one place to another. The warmth and joy of Christmas, brings us closer to each other.',8,18),(50,'1606957146384_068913607.jpg','Do it yourself','There is no \'BUY\', there is only \'DIY\'',7,18),(51,'1606957223736_807756496.jpg','Family holidays','The best of all gifts around any Christmas tree: the presence of a happy family all wrapped up in each other. ',5,18),(52,'1606957364098_655315194.jpg','Who is Santa?','Santa Claus is anyone who loves another and seeks to make them happy; who gives himself by thought or word or deed in every gift that he bestows.',3,18),(53,'1606957451619_632154943.jpg','Joy','It is finally that time of the year!',10,18),(54,'1606957671804_166356558.jpg','Chicago vibes','Christmas isn\'t a season. It\'s a feeling.',11,18),(55,'1606957984537_550018409.jpg','Hallstatt','Hallstatt, a wonderful Austrian village that is so beautiful that its perfect copy was rebuilt in China (so was Colmar in Alsace which was reproduced and rebuilt in Malaysia). In both cases the originals are obviously more authentic.   ',6,10),(56,'1606958038210_222812830.jpg','Tuscany','Tuscany is one of the best places to live a peaceful retirement in Europe. The capital is the beautiful and romantic Florence. Tuscany has other great cities such as the very famous Pisa. Tuscany is obviously one of the most beautiful regions in Italy.',8,10),(57,'1606958130949_747641735.jpg','The Lofoten Islands','The Lofoten Islands are a dream destination for many travellers from all over the world. This archipelago offers one of the most beautiful landscapes in Europe. It is a quiet and peaceful place inhabited by  a few fishermen  who enjoy the fishy waters of Lofoten.',5,10),(58,'1606958286663_983597884.jpg','Best place to kayak','It features crystal turquoise water and the most diverse aquatic life in the world.',3,10),(59,'1606958394651_512751972.jpg','Madagaskar','Madagascar, as the fourth-largest island in the world, has an impressive variety of landscapes to explore. From Baobab Alleys and Spiny Forests to Red and Grey Tsingy Formations. It contains impressive vistas found nowhere else on earth. ',9,10),(60,'1606958517268_910989955.png','Swiss Landscape','A mosaic of grassland, forest, urban areas, and water bodies makes landscapes more productive.',12,10),(61,'1606958670201_790556408.jpg','Colors','Good lighting is essential for photography, and this is especially true with landscape and nature photography. It doesn’t matter how beautiful the location is, if you’re trying to photograph it in bad lighting conditions, the results are not going to be great. And amazing lighting can turn even average scenes into beautiful photographs.',11,10);
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ranks`
--

DROP TABLE IF EXISTS `ranks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ranks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ranks`
--

LOCK TABLES `ranks` WRITE;
/*!40000 ALTER TABLE `ranks` DISABLE KEYS */;
INSERT INTO `ranks` VALUES (1,'Junkie'),(2,'Enthusiast'),(3,'Master'),(4,'Wise and Benevolent Photo Dictator');
/*!40000 ALTER TABLE `ranks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `score` int(11) NOT NULL DEFAULT 0,
  `comment` longtext DEFAULT 'Wrong category',
  `checkbox` tinyint(4) DEFAULT 1,
  `users_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`users_id`,`photo_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_reviews_users1_idx` (`users_id`),
  KEY `fk_reviews_photo1_idx` (`photo_id`),
  CONSTRAINT `fk_reviews_photo1` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reviews_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,10,'Did you know that Cloud Gate was inspired by liquid mercury, the sculpture is among the largest of its kind in the world, measuring 66-feet long by 33-feet high. Amazing photo!',1,1,2),(2,10,'Wow! I like this photo very much!',1,1,1),(3,9,'Plovdiv looks lovely during the autumn!',1,1,6),(4,10,'This photo is amazing! Congratulations!',1,1,7),(5,6,'Hey, what a nice shot!',1,1,3),(6,10,'I love this photo!',1,2,7),(7,5,'Photo is good, but I think you can do more!',1,2,6),(8,9,'Very warm and sunny tones. This photo brings joy!',1,2,3),(9,8,'I would really enjoy the walks there!',1,2,1),(10,10,'Cloud Gate, aka “The Bean”, is one of Chicago\'s most popular sights.',1,2,2),(11,8,'This building is really interesting! Nice shot :))',1,2,4),(12,9,'This house looks really dreamy!',1,2,8),(13,5,'Milano is one of my favourite cities, but this building is not the most modern there!',1,2,5),(14,9,'Impressive building... Thank you for showing it!',1,1,4),(15,5,'This building near UC central is very innovative, but you could take a better shot!',1,1,5),(16,9,'The differences between contemporary and modern design. You might think these two styles are synonymous—both describing design that is au courant—but the reality is these styles have several distinctions.\n\nNice shot though! ;))',1,1,8),(17,10,'Christmas doesn’t come from a store. Maybe Christmas perhaps means a little bit more. And it is shown on this photo!',1,2,32),(18,10,'This doggo should be the winner! Thank you for showing it!',1,1,9),(19,10,'The pug is like a child... the greatest of all dogs ever!',1,1,13),(20,10,'I would really enjoy the walks with this little guy! Wishing you luck!',1,1,13),(21,7,'What a sweet little creature! Good luck!',1,2,18),(22,8,'My all time favourite! I have a pug and he is my good little boy! :-D',1,1,13),(23,8,'This is somehow magical!',1,2,42),(24,8,'Wow! You have captured the moment!',1,2,47),(25,7,'Looks like a great idea!',1,2,46),(26,8,'Makes me want to go skiing :))',1,2,43),(27,8,'This looks soooo delicious!',1,2,49),(28,9,'Ooo what a cute family photo :))',1,2,51),(29,10,'This photo really brings joy, thank you for sharing!',1,2,53),(30,6,'Creative! I also enjoy DIY',1,2,50),(31,7,'It really is! Nice shot!',1,1,44),(32,9,'Looks like a snowy fairytale ^^',1,1,46),(33,7,'What a cute photo!',1,1,24);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Junkie'),(2,'Organizator');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token_blacklist`
--

DROP TABLE IF EXISTS `token_blacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `token_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token_blacklist`
--

LOCK TABLES `token_blacklist` WRITE;
/*!40000 ALTER TABLE `token_blacklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `token_blacklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `types`
--

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` VALUES (1,'open'),(2,'invitational');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(256) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `curr_points` int(11) DEFAULT 0,
  `roles_id` int(11) NOT NULL DEFAULT 1,
  `ranks_id` int(11) NOT NULL DEFAULT 1,
  `next_rank_in` int(11) NOT NULL DEFAULT 0,
  `avatar` varchar(256) DEFAULT '1605565415946_656730001.jpg',
  PRIMARY KEY (`id`,`roles_id`,`ranks_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_users_roles1_idx` (`roles_id`),
  KEY `fk_users_ranks1_idx` (`ranks_id`),
  CONSTRAINT `fk_users_ranks1` FOREIGN KEY (`ranks_id`) REFERENCES `ranks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_roles1` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'antonina','$2b$10$70x1yHIOWdGwZTQzo9RL7evWquQPiZfT5rTYspRPLN6p8duc2P9q2','Antonina','Dermendjieva',0,2,1,0,'1606860275239_038712173.jpg'),(2,'vzasheva','$2b$10$p2Zy.2lTfct3ypqKSw6Wf.PurztVTR2dxPKZbrChl3KWLLvgsLkW6','Violina','Zasheva',0,2,1,0,'1606948174261_619151397.png'),(3,'photo_junkie1','$2b$10$WRQt6A.Lz6stwVeGK6O3dOFBPfQb7f/w..GuWDojAeM6luHj4Eh6O','Iliana','Stoyanova',7,1,1,0,'1606898588138_154218468.jpg'),(4,'bruno','$2b$10$i0iMH9G9C624LnSmttsZHuPpIQZ7XYFLHXaUPi4lFzbpHPNjSbCU2','Bruno','Iliev',979,1,3,0,'1606942215061_695896265.png'),(5,'petya','$2b$10$V7omESx7OAGKk.osdNaWMedgLZr/2apW8FRmpt5AFL4RyFWnf4PyG','Petya','Atanasova',8,1,1,0,'1606920849965_248212601.jpg'),(6,'junkie1','$2b$10$vjmWwkUh7O1h5XE9qkYtOuL4nHIm2mCOQzCeuXPWKepR//T453vMO','Alexander','Ivanov',844,1,3,0,'1605565415946_656730001.jpg'),(7,'edward','$2b$10$cBMuMmHYSWD/6eHtwc5dSuRjzhT6rHo4mah8xZrWWwtIiJiVA6SV.','Edward','Evlogiev',9,1,1,0,'1605565415946_656730001.jpg'),(8,'violina','$2b$10$NhSIrH/oXu1lk/7nL78v/.Bcbkt0E/q9ECHr9CCOyI.u5uFv7AQYW','Violina','Zasheva',59,1,2,0,'1606941553568_613312581.jpg'),(9,'giuseppe','$2b$10$sYPEliIZxouC.873eLMj5.fc0fCQq2g1a3D6hkeosYvYxPROLwl7.','Giuseppe','Conte',4,1,1,0,'1606949010948_222614133.jpg'),(10,'student','$2b$10$5XKLDQDVv5yEH3rW.SV.Mu/2rFVINuV2LRXhfJyaPHAiblvH4c5ju','Telerik JS','Student',2,1,1,0,'1605565415946_656730001.jpg'),(11,'developer','$2b$10$1eTzQUh2tUhQvossIVO1M.lSfQzsDlks2y0d1w61AT8w7FVbWXC7G','Front-end','Developer',3,1,1,0,'1605565415946_656730001.jpg'),(12,'krump','$2b$10$hWsI6zmmB7dglm/hiH20zO9Rq7Ms2BKakMThGzayUwTs5NBYZ814S','Krum','Pashov',2,1,1,0,'1605565415946_656730001.jpg');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_are_invited`
--

DROP TABLE IF EXISTS `users_are_invited`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_are_invited` (
  `contest_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`contest_id`,`users_id`),
  KEY `fk_contest_has_users_users1_idx` (`users_id`),
  KEY `fk_contest_has_users_contest1_idx` (`contest_id`),
  CONSTRAINT `fk_contest_has_users_contest1` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contest_has_users_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_are_invited`
--

LOCK TABLES `users_are_invited` WRITE;
/*!40000 ALTER TABLE `users_are_invited` DISABLE KEYS */;
INSERT INTO `users_are_invited` VALUES (16,4),(17,4),(18,4),(19,4),(20,4),(21,4);
/*!40000 ALTER TABLE `users_are_invited` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_are_jury`
--

DROP TABLE IF EXISTS `users_are_jury`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_are_jury` (
  `users_id` int(11) NOT NULL,
  `contests_id` int(11) NOT NULL,
  PRIMARY KEY (`contests_id`,`users_id`),
  KEY `fk_users_has_contests_contests1_idx` (`contests_id`),
  KEY `fk_users_has_contests_users1_idx` (`users_id`),
  CONSTRAINT `fk_users_has_contests_contests1` FOREIGN KEY (`contests_id`) REFERENCES `contests` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_contests_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_are_jury`
--

LOCK TABLES `users_are_jury` WRITE;
/*!40000 ALTER TABLE `users_are_jury` DISABLE KEYS */;
INSERT INTO `users_are_jury` VALUES (4,17),(4,18),(4,19),(4,20);
/*!40000 ALTER TABLE `users_are_jury` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_has_contest`
--

DROP TABLE IF EXISTS `users_has_contest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_has_contest` (
  `users_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `has_uploaded` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`users_id`,`contest_id`),
  KEY `fk_users_has_contest_contest1_idx` (`contest_id`),
  KEY `fk_users_has_contest_users1_idx` (`users_id`),
  CONSTRAINT `fk_users_has_contest_contest1` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_contest_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_has_contest`
--

LOCK TABLES `users_has_contest` WRITE;
/*!40000 ALTER TABLE `users_has_contest` DISABLE KEYS */;
INSERT INTO `users_has_contest` VALUES (3,1,1),(3,2,1),(3,3,0),(3,4,1),(3,5,1),(3,6,1),(3,10,1),(3,18,1),(4,1,1),(4,2,1),(4,3,1),(4,4,1),(4,5,1),(4,6,1),(4,7,1),(4,8,1),(4,9,1),(4,10,1),(4,12,1),(5,1,1),(5,2,1),(5,3,1),(5,4,1),(5,5,1),(5,6,1),(5,10,1),(5,18,1),(6,1,1),(6,2,1),(6,3,1),(6,4,1),(6,5,1),(6,6,1),(6,10,1),(6,18,1),(7,3,1),(7,5,1),(7,6,1),(7,7,1),(7,8,1),(7,9,1),(7,10,1),(7,11,1),(7,18,1),(8,3,1),(8,4,0),(8,6,1),(8,10,1),(8,12,1),(8,18,1),(9,3,1),(9,6,1),(9,10,1),(9,12,1),(10,3,1),(10,18,1),(11,3,1),(11,10,1),(11,18,1),(12,3,1),(12,10,1);
/*!40000 ALTER TABLE `users_has_contest` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-03  3:40:33
